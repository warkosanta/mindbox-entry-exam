drop table if exists Products

drop table if exists Categories

drop table if exists ProductsCategories

create table Products(
	ProductID int primary key,
	Name varchar(255)
)

create table Categories(
	CategoriesID int primary key,
	Name varchar(255)
)

create table ProductsCategories(
	CategoriesID int,
	ProductID int,
	primary key (CategoriesID, ProductID)
)

insert into Products (ProductID, Name)
values (1, '����� �������������'),
	   (2, '�������'),
	   (3, '�����'),
	   (4, '����')

insert into Categories (CategoriesID, Name)
values (1, '��������'),
	   (2, '������'),
	   (3, '�������')

insert into ProductsCategories (CategoriesID, ProductID)
values (1, 1),
	   (1, 3),
	   (2, 2),
	   (3, 1)