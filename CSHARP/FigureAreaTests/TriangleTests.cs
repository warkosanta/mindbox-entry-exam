﻿using System;
using FigureArea.Figures;
using FigureArea.Interfaces;
using Xunit;

namespace FigureAreaTests
{
    public class TriangleTests
    {

        [Theory]
        [InlineData(6, 10, 14, 25.98076211353316)]
        [InlineData(5, 5, 5, 10.825317547305483)]
        [InlineData(100, 250, 176, 6963.1055571490515)]
        [InlineData(12306, 67870, 60099, 303854329.52282554)]
        public void GetArea_IntegerSides_ReturnsExpectedArea(int a, int b, int c, double expected)
        {
            // Arrange
            IFigure figure = new Triangle(a, b, c);

            // Act
            var actual = figure.GetArea();

            // Assert
            Assert.Equal(expected, actual);
        }


        [Theory]
        [InlineData(7.101, 0.904, 7.6, 2.7651619646253924)]
        [InlineData(158.0005, 67.1234, 100.5, 2161.2090896537707)]
        [InlineData(6234, 967.8910, 5432.99, 1579262.7804224908)]
        [InlineData(0.0801, 0.04509, 0.056, 0.0012234312316742706)]
        public void GetArea_DoubleSides_ReturnsExpectedArea(double a, double b, double c, double expected)
        {
            // Arrange
            IFigure figure = new Triangle(a, b, c);

            // Act
            var actual = figure.GetArea();

            // Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(6, 10, 16)]
        [InlineData(0, 0, 0)]
        [InlineData(2, 2, 4)]
        [InlineData(6.0001, 0.004, 7)]
        [InlineData(-1, -5, -6)]
        [InlineData(-7.101, 0.904, -7.6)]
        public void GetArea_IncorrectSides_ThrowsArgumentOutOfRangeException(double a, double b, double c)
        {
            // Arrange
            IFigure figure;

            // Act
            Action act = () => figure = new Triangle(a, b, c);

            // Assert
            Assert.Throws<ArgumentOutOfRangeException>(act);
        }

        [Theory]
        [InlineData(3, 4, 5, true)]
        [InlineData(5, 4, 3, true)]
        [InlineData(4, 5, 3, true)]
        [InlineData(100, 250, 176, false)]
        [InlineData(7.101, 0.904, 7.6, false)]
        [InlineData(223.44645, 158.0005, 158.0005, true)]
        public void IsRectangular_Sides_ReturnsExpectedBoolean(double a, double b, double c, bool expected)
        {
            // Arrange
            var figure = new Triangle(a, b, c);

            // Act
            var actual = figure.IsRectangular();

            // Assert
            Assert.Equal(expected, actual);
        }

    }
}