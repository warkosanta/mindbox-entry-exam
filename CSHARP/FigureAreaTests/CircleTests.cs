﻿using System;
using FigureArea.Figures;
using FigureArea.Interfaces;
using Xunit;

namespace FigureAreaTests
{
    public class CircleTests
    {

        [Theory]
        [InlineData(0, 0)]
        [InlineData(1, 3.141592653589793)]
        [InlineData(10, 314.1592653589793)]
        [InlineData(50, 7853.981633974483)]
        [InlineData(333, 348368.06776391855)]
        public void GetArea_IntegerRadius_ReturnsExpectedArea(int r, double expected)
        {
            // Arrange
            IFigure figure = new Circle(r);

            // Act
            var actual = figure.GetArea();

            // Assert
            Assert.Equal(expected, actual);
        }


        [Theory]
        [InlineData(0.01, 0.0003141592653589793)]
        [InlineData(1.1, 3.8013271108436504)]
        [InlineData(15.005, 707.3296644955583)]
        [InlineData(99.1099, 30859.14922648169)]
        [InlineData(105.70906, 35105.429806322594)]
        public void GetArea_DoubleRadius_ReturnsExpectedArea(double r, double expected)
        {
            // Arrange
            IFigure figure = new Circle(r);

            // Act
            var actual = figure.GetArea();

            // Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(-90000)]
        [InlineData(-1.111)]
        [InlineData(-0.00001)]
        [InlineData(double.MinValue)]
        public void GetArea_NegativeRadius_ThrowsArgumentOutOfRangeException(double r)
        {
            // Arrange
            IFigure figure;

            // Act
            Action act = () => figure = new Circle(r);

            // Assert
            Assert.Throws<ArgumentOutOfRangeException>(act);
        }

    }
}