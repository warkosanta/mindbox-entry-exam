﻿namespace FigureArea.Interfaces
{
    /// <summary>
    /// Интерфейс калькулятора фигуры
    /// </summary>
    public interface IFigureCalculator
    {

        /// <summary>
        /// Расчитать площадь фигуры
        /// </summary>
        /// <param name="figure">Фигура</param>
        /// <returns>Площадь фигуры числом с плавающей точкой</returns>
        public double CalculateFigureArea(IFigure figure);

    }
}