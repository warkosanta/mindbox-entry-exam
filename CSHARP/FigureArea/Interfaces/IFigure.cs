﻿namespace FigureArea.Interfaces
{
    /// <summary>
    /// Интерфейс фигуры
    /// </summary>
    public interface IFigure
    {

        /// <summary>
        /// Расчитать площадь фигуры
        /// </summary>
        /// <returns>Площадь фигуры числом с плавающей точкой</returns>
        public double GetArea();

    }
}