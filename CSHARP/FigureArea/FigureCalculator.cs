﻿using FigureArea.Interfaces;

namespace FigureArea
{
    /// <summary>
    /// Калькулятор фигуры
    /// </summary>
    /// <remarks>Класс и интерфейс выделены на случай если необходимо преобразовать результат расчета площади,
    /// например округлить или привести к целочисленному результату.
    /// Также данный класс демонстрирует вычисление площади фигуры без знания типа фигуры в compile-time</remarks>
    public class FigureCalculator : IFigureCalculator
    {

        /// <inheritdoc />
        public double CalculateFigureArea(IFigure figure)
        {
            return figure.GetArea();
        }

    }
}