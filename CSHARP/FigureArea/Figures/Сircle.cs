﻿using FigureArea.Interfaces;

namespace FigureArea.Figures
{
    /// <summary>
    /// Фигура круга
    /// </summary>
    public class Circle : IFigure
    {

        /// <summary>
        /// Радиус круга
        /// </summary>
        private readonly double _r;

        /// <summary>
        /// Публичный конструктор с параметром
        /// </summary>
        /// <param name="r">Радиус круга</param>
        public Circle(double r)
        {
            if (r < 0) throw new ArgumentOutOfRangeException(nameof(r));
            _r = r;
        }

        /// <inheritdoc />
        /// <see cref="https://en.wikipedia.org/wiki/Area_of_a_circle">Формула расчета площади круга</see>
        public double GetArea()
        {
            return Math.PI * Math.Pow(_r, 2);
        }

    }
}