﻿using FigureArea.Interfaces;

namespace FigureArea.Figures
{
    /// <summary>
    /// Фигура треугольник
    /// </summary>
    public class Triangle : IFigure
    {

        /// <summary>
        /// Точность сравнения
        /// </summary>
        private const double Accuracy = 0.0001;

        /// <summary>
        /// Первая сторона треугольника
        /// </summary>
        private readonly double _a;

        /// <summary>
        /// Вторая сторона треугольника
        /// </summary>
        private readonly double _b;

        /// <summary>
        /// Третья сторона треугольника
        /// </summary>
        private readonly double _c;

        /// <summary>
        /// Публичный конструктор с параметрами
        /// </summary>
        /// <param name="a">Первая сторона треугольника</param>
        /// <param name="b">Вторая сторона треугольника</param>
        /// <param name="c">Третья сторона треугольника</param>
        public Triangle(double a, double b, double c)
        {
            if (a < 0) throw new ArgumentOutOfRangeException(nameof(a));
            if (b < 0) throw new ArgumentOutOfRangeException(nameof(b));
            if (c < 0) throw new ArgumentOutOfRangeException(nameof(c));

            if (a + b <= c || a + c <= b || c + b <= a)
                throw new ArgumentOutOfRangeException(
                    "Сумма длин двух любых сторон треугольника не должна быть меньше длины третьей стороны.");

            _a = a;
            _b = b;
            _c = c;
        }

        /// <summary>
        /// Полупериметр
        /// </summary>
        /// <see cref="https://en.wikipedia.org/wiki/Semiperimeter">
        /// Формула расчета полупериметра
        /// </see>
        private double Semiperimeter => (_a + _b + _c) / 2;

        /// <inheritdoc />
        /// <see cref="https://en.wikipedia.org/wiki/Triangle#Computing_the_area_of_a_triangle">
        /// Формула расчета площади треугольника по формуле Герона
        /// </see>
        public double GetArea()
        {
            return Math.Sqrt(Semiperimeter
                             * (Semiperimeter - _a)
                             * (Semiperimeter - _b)
                             * (Semiperimeter - _c));
        }

        /// <summary>
        /// Проверка является ли треугольник прямоугольным
        /// </summary>
        public bool IsRectangular()
        {
            var a = Math.Pow(_a, 2);
            var b = Math.Pow(_b, 2);
            var c = Math.Pow(_c, 2);

            return CheckAccuracy(a + b, c)
                   || CheckAccuracy(a + c, b)
                   || CheckAccuracy(b + c, a);
        }

        /// <summary>
        /// Сравнение разности двух чисел и допустимой точности
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        private bool CheckAccuracy(double a, double b)
        {
            return Math.Abs(a - b) < Accuracy;
        }

    }
}